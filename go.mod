module gitlab.com/holi0317/toolbox

go 1.18

require (
	github.com/manifoldco/promptui v0.9.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
	github.com/urfave/cli/v2 v2.8.1
)

require (
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
)
