package cmd

import (
	"github.com/urfave/cli/v2"
)

func App() *cli.App {
	app := cli.NewApp()

	app.Name = "toolbox"
	app.Description = "Box of small utility commands"

	app.Commands = []*cli.Command{
		ffsplitCmd(),
	}

	return app
}
