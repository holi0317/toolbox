package cmd

import (
	"errors"
	"io/fs"
	"os"
	"os/exec"

	"github.com/manifoldco/promptui"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/holi0317/toolbox/ffsplit"
)

func yesNo(label string) bool {
	prompt := promptui.Select{
		Label: label + "[Yes/No]",
		Items: []string{"Yes", "No"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		logrus.WithError(err).Fatal("Prompt failed")
	}
	return result == "Yes"
}

func ffsplitCmd() *cli.Command {
	var configPath string
	var dstFolderPath string
	var isInteractive bool

	return &cli.Command{
		Name:      "ffsplit",
		Usage:     "Split given video into multiple segments of videos.",
		ArgsUsage: "An input video that can be understand by ffmpeg",
		Description: `This command accept a video input and a configuration file. The command
will then split the video input into multiple segments with ffmpeg according to the configuration file,
the output to output directory.

The configuration file is a line-separated. Where each line will represent a video segment to output.
Each line must have a start and end timestamp, separated by some space. And an optional "prefix"
that control the output filename's prefix.
Timestamp format is HH:MM:SS. Except the seconds part, all parts are optional. Seconds/minutes that exceed
60 will still be considered a valid timestamp.`,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "config",
				Value:       "ffsplit",
				Usage:       "Config file path for splitting",
				Destination: &configPath,
			},
			&cli.StringFlag{
				Name:        "dest",
				Value:       "export",
				Usage:       "File path to export directory",
				Destination: &dstFolderPath,
			},
			&cli.BoolFlag{
				Name:        "interactive",
				Aliases:     []string{"i"},
				Value:       false,
				Usage:       "Run interactively. This will first start text editor for editing ffplsit config and play the video. Then run ffsplit. Finally play the output videos for validation",
				Destination: &isInteractive,
			},
		},
		Category: "ffmpeg",
		Action: func(c *cli.Context) error {
			args := c.Args()

			if !args.Present() {
				err := cli.ShowSubcommandHelp(c)
				if err != nil {
					return err
				}
				return cli.Exit("", 2)
			}

			if args.Len() > 1 {
				return errors.New("ffsplit command only accept 1 and only 1 argument (Source video file) only.")
			}

			src := args.First()

			if isInteractive {
				logrus.Info("Building config interactively")
				err := ffsplit.Interactive(c.Context, src, configPath)
				if err != nil {
					return err
				}
			}

			logrus.WithField("configPath", configPath).Info("Reading config file")
			file, err := os.Open(configPath)
			if err != nil {
				return err
			}
			defer file.Close()

			conf, err := ffsplit.ParseConfig(file, src, dstFolderPath)
			if err != nil {
				return err
			}

			logrus.WithField("conf_len", len(conf.Entries)).Info("Read and parsed config file")
			if len(conf.Entries) == 0 {
				logrus.Info("Empty config file. Aborting")
				return nil
			}

			logrus.WithFields(logrus.Fields{
				"src":       src,
				"dstfolder": dstFolderPath,
			}).Info("Generating ffmpeg commands")
			cmds := conf.MakeCommand(c.Context)
			logrus.WithField("cmds", cmds).Info("Generated conversion commands")

			_, err = os.Stat(dstFolderPath)
			if errors.Is(err, fs.ErrNotExist) {
				logrus.WithField("dstFolderPath", dstFolderPath).Info("Destination folder does not exist. Creating the folder")
				err := os.Mkdir(dstFolderPath, 0o755)
				if err != nil {
					return err
				}
			}

			for _, cmd := range cmds {
				log := logrus.WithField("cmd", cmd.Args)
				log.Info("Running ffmpeg command")

				// Need to call Output instead of Run to capture stderr without additional code
				cmd.Stderr = nil
				_, err := cmd.Output()

				if err != nil {
					if v, ok := err.(*exec.ExitError); ok {
						stderr := string(v.Stderr)
						log.Warn(stderr)
					}
					return err
				}
				log.Info("Ffmpeg command returned succeed")
			}

			if isInteractive {
				logrus.Info("Playing output videos")

				err := conf.PlayOutputFiles()
				if err != nil {
					return err
				}

				if yesNo("Delete source file?") {
					err := os.Remove(src)
					if err != nil {
						return err
					}
				}

			}

			return nil
		},
	}
}
