build:
	goreleaser build --snapshot --rm-dist --single-target

test:
	go test ./...

lint:
	golangci-lint run -v

clean:
	rm -rf target
