package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/holi0317/toolbox/cmd"
)

func main() {
	app := cmd.App()

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("Failed")
	}
}
