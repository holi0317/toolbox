package ffsplit

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os/exec"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

type ConfigEntry struct {
	StartTime time.Duration
	EndTime   time.Duration
	Prefix    string
}

func (entry *ConfigEntry) DstPath(conf *Config, i int) string {
	_, filename := path.Split(conf.Src)
	ext := path.Ext(filename)
	filenameWithoutExt := strings.TrimSuffix(filename, ext)

	// {prefix}{filename}-{index}.{ext}
	dstfilename := fmt.Sprintf("%s%s-%02d%s", entry.Prefix, filenameWithoutExt, i, ext)
	return filepath.Join(conf.DstFolder, dstfilename)
}

type Config struct {
	// Config entries this holds
	Entries []ConfigEntry

	// Source video file path
	Src string

	// Destination folder path
	DstFolder string
}

// praseTime converts given string timestamp into time.Duration format.
//
// The expected format for timestamp is hh:mm:ss, mm:ss or ss. If the timestamp
// segment begins with 0 (for example, 01), then the 0 can be omitted.
//
// This will not raise error if the time range is incorrect. For example, `2:63` is
// a valid timestamp. Which will resolve to 3 minutes and 3 seconds.
func parseTime(t string) (time.Duration, error) {
	log := logrus.WithField("t", t)

	tokens := strings.Split(t, ":")
	tokens_len := len(tokens)

	log.WithFields(logrus.Fields{
		"tokens":     tokens,
		"tokens_len": tokens_len,
	}).Debug("Parsing timestamp")

	if tokens_len == 0 {
		return 0, fmt.Errorf("Cannot parse empty timestamp: %s", t)
	}

	if tokens_len > 3 {
		return 0, fmt.Errorf("Too many ':' in timestamp: %s", t)
	}

	var sec, min, hour int
	var err error

	// FIXME: Refactor this to become less verbose
	if tokens_len == 1 {
		sec, err = strconv.Atoi(tokens[0])
		if err != nil {
			return 0, fmt.Errorf("Cannot parse second part of timestamp %s, %w", t, err)
		}
	} else if tokens_len == 2 {
		sec, err = strconv.Atoi(tokens[1])
		if err != nil {
			return 0, fmt.Errorf("Cannot parse second part of timestamp %s, %w", t, err)
		}

		min, err = strconv.Atoi(tokens[0])
		if err != nil {
			return 0, fmt.Errorf("Cannot parse minute part of timestamp %s, %w", t, err)
		}
	} else {
		sec, err = strconv.Atoi(tokens[2])
		if err != nil {
			return 0, fmt.Errorf("Cannot parse second part of timestamp %s, %w", t, err)
		}

		min, err = strconv.Atoi(tokens[1])
		if err != nil {
			return 0, fmt.Errorf("Cannot parse minute part of timestamp %s, %w", t, err)
		}

		hour, err = strconv.Atoi(tokens[0])
		if err != nil {
			return 0, fmt.Errorf("Cannot parse hour part of timestamp %s, %w", t, err)
		}
	}

	log.WithFields(logrus.Fields{
		"sec":  sec,
		"min":  min,
		"hour": hour,
	}).Debug("Extracted components from timestamp")

	duration := time.Duration(hour*int(time.Hour) + min*int(time.Minute) + sec*int(time.Second))

	log.WithField("duration_sec", duration.Seconds()).Debug("Parsed timestamp")

	return duration, nil
}

func ParseConfig(reader io.Reader, src string, dstFolder string) (*Config, error) {
	scanner := bufio.NewScanner(reader)

	config := &Config{
		Entries:   make([]ConfigEntry, 0),
		Src:       src,
		DstFolder: dstFolder,
	}

	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Fields(line)

		log := logrus.WithField("line", line)
		log.WithField("fields", fields).Debug("Parsing config line")

		if len(fields) == 0 {
			log.Debug("Line is empty. Ignoring")
			continue
		}

		if len(fields) < 2 || len(fields) > 3 {
			return nil, fmt.Errorf("Invalid ffsplit config format: %s", line)
		}

		var err error

		entry := ConfigEntry{}

		entry.StartTime, err = parseTime(fields[0])
		if err != nil {
			return nil, fmt.Errorf("Failed to parse start time, %w", err)
		}

		entry.EndTime, err = parseTime(fields[1])
		if err != nil {
			return nil, fmt.Errorf("Failed to parse end time, %w", err)
		}

		if len(fields) == 3 {
			entry.Prefix = fields[2]
			log.WithField("prefix", entry.Prefix).Debug("Got prefix from line")
		}

		log.WithField("entry", entry).Debug("Parsed config line")

		config.Entries = append(config.Entries, entry)
	}

	err := scanner.Err()
	if err != nil {
		return nil, err
	}

	logrus.WithField("config", config).Debug("Parsed all config lines")

	return config, nil
}

func ffmpegDuration(dur time.Duration) string {
	ms := dur.Milliseconds()
	return strconv.FormatInt(ms, 10) + "ms"
}

// Create ffmpeg commands for running the transformation.
//
// The returned commands (exec.Cmd) have not been spawned yet. Call `Run` function
// to execute the command.
func (conf *Config) MakeCommand(ctx context.Context) []*exec.Cmd {
	res := make([]*exec.Cmd, 0)

	for i, segment := range conf.Entries {
		dstpath := segment.DstPath(conf, i)

		// ffmpeg -ss {start} -i {src} -t {delta} -c copy {dst}
		cmd := exec.CommandContext(
			ctx,
			"ffmpeg",
			"-ss", ffmpegDuration(segment.StartTime),
			"-i", conf.Src,
			"-to", ffmpegDuration(segment.EndTime-segment.StartTime),
			"-c", "copy",
			dstpath,
		)

		res = append(res, cmd)
	}

	return res
}

// List all output file paths
func (conf *Config) outputFiles() []string {
	res := make([]string, 0)

	for i, segment := range conf.Entries {
		res = append(res, segment.DstPath(conf, i))
	}

	return res
}

// Play all output files in mpv.
// This will return after user closes mpv
func (conf *Config) PlayOutputFiles() error {
	outputFiles := conf.outputFiles()

	cmd := exec.Command("mpv", outputFiles...)
	err := cmd.Run()
	return err
}
