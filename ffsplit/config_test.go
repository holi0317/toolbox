package ffsplit

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseConfigEmpty(t *testing.T) {
	body := ""
	reader := strings.NewReader(body)

	config, err := ParseConfig(reader, "a", "b")

	require.Nil(t, err, "Error should be nil")
	require.Len(t, config.Entries, 0, "Should have no config entry")
	require.Equal(t, config.Src, "a", "Should match src")
	require.Equal(t, config.DstFolder, "b", "Should match dstFolder")
}

func TestParseConfigIgnoreWhiteline(t *testing.T) {
	body := `

	`
	reader := strings.NewReader(body)

	config, err := ParseConfig(reader, "", "")

	require.Nil(t, err, "Error should be nil")
	require.Len(t, config.Entries, 0, "Should have no config entry")
}

func TestParseConfigSuccess(t *testing.T) {
	body := `
	15:23 16:28 asdf
	18:0 23:65

	`
	reader := strings.NewReader(body)

	config, err := ParseConfig(reader, "", "")

	require.Nil(t, err, "Error should be nil")
	require.Len(t, config.Entries, 2, "Should have 2 config entry")

	one := config.Entries[0]
	require.Equal(t, 15*60+23, int(one.StartTime.Seconds()))
	require.Equal(t, 16*60+28, int(one.EndTime.Seconds()))
	require.Equal(t, "asdf", one.Prefix)

	two := config.Entries[1]
	require.Equal(t, 18*60+0, int(two.StartTime.Seconds()))
	require.Equal(t, 23*60+65, int(two.EndTime.Seconds()))
	require.Equal(t, "", two.Prefix)
}

func TestParseConfigTooManyFields(t *testing.T) {
	body := `
	1:0 2:8 asdf moremoremroe
	`
	reader := strings.NewReader(body)

	_, err := ParseConfig(reader, "", "")

	require.Error(t, err, "Error should be nil")
}

func TestMakeCommandEmpty(t *testing.T) {
	var conf Config

	cmds := conf.MakeCommand(context.Background())

	require.Len(t, cmds, 0)
}
