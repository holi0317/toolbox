package ffsplit

import (
	"context"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"os/exec"

	"github.com/sirupsen/logrus"
)

func emptyFile(name string) error {
	file, err := os.Create(name)
	if err != nil {
		return err
	}
	defer file.Close()

	return nil
}

func playVideo(ctx context.Context, name string) (*exec.Cmd, error) {
	cmd := exec.CommandContext(ctx, "mpv", name)
	cmd.Stdin = nil
	cmd.Stdout = nil
	cmd.Stderr = nil

	err := cmd.Start()

	return cmd, err
}

func editFile(ctx context.Context, name string) (*exec.Cmd, error) {
	editor := os.Getenv("EDITOR")
	if editor == "" {
		editor = "vim"
	}

	cmd := exec.CommandContext(ctx, editor, name)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Start()

	return cmd, err
}

// Make config interactively.
// The config will be available for parsing at the given configPath after this function completes
func Interactive(ctx context.Context, src string, configPath string) error {
	log := logrus.WithContext(ctx).WithFields(logrus.Fields{
		"src":        src,
		"configPath": configPath,
	})

	// Ensure src exist
	_, err := os.Stat(src)
	if errors.Is(err, fs.ErrNotExist) {
		return fmt.Errorf("Source file %s does not exist. %w", src, err)
	}

	// Prepare config file
	log.Debug("Truncating config file")
	err = emptyFile(configPath)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Play video
	log.Debug("Spawning video player")
	_, err = playVideo(ctx, src)
	if err != nil {
		return fmt.Errorf("Failed to play video at path %s. %w", src, err)
	}

	// Start editor
	log.Debug("Spawning file editor")
	cmd, err := editFile(ctx, configPath)
	if err != nil {
		return fmt.Errorf("Failed to spawn editor. %w", err)
	}

	// Wait for editor to complete
	err = cmd.Wait()
	if err != nil {
		return fmt.Errorf("Editor returned error. Aborting. %w", err)
	}

	// Cancel running (if any) play video command
	log.Debug("Editor completed. Killing video player")
	cancel()

	return nil
}
